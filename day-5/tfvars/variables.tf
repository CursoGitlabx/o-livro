variable "ec2_region" {
    default = "sa-east-1"
}

variable "ec2_keypair_name" {
    default = "descomplicando_gitlab"
}

variable "ec2_instance_type" {
    default = "t2.micro"
}

variable "ec2_image_id" {
    default = "ami-0e66f5495b4efdd0f"
}

variable "ec2_tags" {
    default = "Teste do Gitlab"
}

variable "ec2_instance_count" {
    default = 1
}